from Common import BasePage
from selenium.webdriver.common.by import By


class HotelLoginPage(BasePage):
    url = 'http://localhost:8080/#/login'
    xpath = {
        'back': (By.XPATH, '//*[@id="app"]/div[1]/a/h3[1]'),
        'title': (By.XPATH, '//*[@id="app"]/div[2]/div[2]/h1'),
        'ipt_user': (By.XPATH, '//*[@id="app"]/div[2]/div[2]/div[1]/input'),
        'ipt_password': (By.XPATH, '//*[@id="app"]/div[2]/div[2]/div[2]/input'),
        'btn_commit': (By.XPATH, '//*[@id="app"]/div[2]/div[2]/input'),
        'img': (By.XPATH, '//*[@id="app"]/div[2]/div[1]/img')
    }

    # 进入hotel-login页面
    def open_hotel_login_page(self):
        self.open_page_and_max(self.url)

    # 获取页面 back 按钮
    def get_page_back_el(self):
        el = self.find_el(loc=self.xpath['back'])
        font = self.get_el_font(loc=self.xpath['back'])
        return el, font

    # 获取页面 title 按钮
    def get_page_title_el(self):
        el = self.find_el(loc=self.xpath['title'])
        font = self.get_el_font(loc=self.xpath['title'])
        return el, font

    # 输入 user
    def set_ipt_user(self, content):
        self.send_keys_to_el(loc=self.xpath['ipt_user'], content=content)

    # 输入 password
    def set_ipt_password(self, content):
        self.send_keys_to_el(loc=self.xpath['ipt_password'], content=content)

    # 点击提交
    def commit_login(self):
        self.click_to_el(loc=self.xpath['btn_commit'])

    # 等待图片加载完成
    def await_img(self):
        self.await_img_loaded(loc=self.xpath['img'])