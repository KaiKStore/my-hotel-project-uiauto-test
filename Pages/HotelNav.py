from Common import BasePage
from selenium.webdriver.common.by import By


class HotelNavPage(BasePage):
    url = 'http://localhost:8080/#/'
    xpath = {
        'page_title': (By.XPATH, '//*[@id="app"]/div/h1'),
        'nav_register': (By.XPATH, '//*[@id="app"]/div/div/div/div[1]/a'),
        'nav_switch_user': (By.XPATH, '//*[@id="app"]/div/div/div/div[2]/a'),
        'nav_login': (By.XPATH, '//*[@id="app"]/div/div/div/div[2]/a'),
        'nav_hotel': (By.XPATH, '//*[@id="app"]/div/div/div/div[3]/a'),
        'nav_user': (By.XPATH, '//*[@id="app"]/div/div/div/div[4]/a'),
    }

    # 进入hotel-nav页面
    def open_hotel_nav_page(self):
        self.open_page_and_max(self.url)

    # 获取页码欢迎标题元素
    def get_page_title_el(self):
        el = self.find_el(loc=self.xpath['page_title'])
        font = self.get_el_font(loc=self.xpath['page_title'])
        return el, font

    # 获取注册按钮元素
    def get_btn_register_el(self):
        el = self.find_el(loc=self.xpath['nav_register'])
        font = self.get_el_font(loc=self.xpath['nav_register'])
        return el, font

    # 点击注册按钮
    def click_btn_register(self):
        self.click_to_el(loc=self.xpath['nav_register'])

    # 获取登录按钮元素（未登录、已登录适用）
    def get_btn_login_el(self):
        el = self.find_el(loc=self.xpath['nav_switch_user'])
        font = self.get_el_font(loc=self.xpath['nav_switch_user'])
        return el, font

    # 点击切换用户按钮
    def click_btn_switch_user(self):
        self.click_to_el(loc=self.xpath['nav_switch_user'])

    # 点击登录按钮
    def click_btn_login(self):
        self.click_to_el(loc=self.xpath['nav_login'])

    # 获取住酒店按钮元素
    def get_btn_hotel_el(self):
        el = self.find_el(loc=self.xpath['nav_hotel'])
        font = self.get_el_font(loc=self.xpath['nav_hotel'])
        return el, font

    # 点击住酒店按钮
    def click_btn_hotel(self):
        self.click_to_el(loc=self.xpath['nav_hotel'])

    # 获取用户按钮元素
    def get_btn_user_el(self):
        el = self.find_el(loc=self.xpath['nav_user'])
        font = self.get_el_font(loc=self.xpath['nav_user'])
        return el, font

    # 点击用户按钮
    def click_btn_user(self):
        self.click_to_el(loc=self.xpath['nav_user'])