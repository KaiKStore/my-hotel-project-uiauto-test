from Common import BasePage
from selenium.webdriver.common.by import By


class HotelRegisterPage(BasePage):
    url = 'http://localhost:8080/#/register'
    xpath = {
        'back': (By.XPATH, '//*[@id="app"]/div[1]/a'),
        'title': (By.XPATH, '//*[@id="app"]/div[2]/div[1]/h1'),
        'ipt_user': (By.XPATH, '//*[@id="app"]/div[2]/div[1]/div[1]/input'),
        'ipt_name': (By.XPATH, '//*[@id="app"]/div[2]/div[1]/div[2]/input'),
        'ipt_password': (By.XPATH, '//*[@id="app"]/div[2]/div[1]/div[3]/input'),
        'ipt_phone': (By.XPATH, '//*[@id="app"]/div[2]/div[1]/div[4]/input'),
        'gender_select': (By.XPATH, '//*[@id="app"]/div[2]/div[1]/div[5]/select'),
        'gender_man': (By.XPATH, '//*[@id="app"]/div[2]/div[1]/div[5]/select/option[1]'),
        'gender_woman': (By.XPATH, '//*[@id="app"]/div[2]/div[1]/div[5]/select/option[2]'),
        'btn_commit': (By.XPATH, '//*[@id="app"]/div[2]/div[1]/input'),
        'img': (By.XPATH, '//*[@id="app"]/div[2]/div[2]/img')
    }

    # 进入注册页
    def open_hotel_register_page(self):
        self.open_page_and_max(self.url)

    # 获取页面 back 按钮
    def get_page_back_el(self):
        el = self.find_el(loc=self.xpath['back'])
        font = self.get_el_font(loc=self.xpath['back'])
        return el, font

    # 获取页面 title 按钮
    def get_page_title_el(self):
        el = self.find_el(loc=self.xpath['title'])
        font = self.get_el_font(loc=self.xpath['title'])
        return el, font

    # 输入 user
    def set_ipt_user(self, content):
        self.send_keys_to_el(loc=self.xpath['ipt_user'], content=content)

    # 输入 name
    def set_ipt_name(self, content):
        self.send_keys_to_el(loc=self.xpath['ipt_name'], content=content)

    # 输入 password
    def set_ipt_password(self, content):
        self.send_keys_to_el(loc=self.xpath['ipt_password'], content=content)

    # 输入 phone
    def set_ipt_phone(self, content):
        self.send_keys_to_el(loc=self.xpath['ipt_phone'], content=content)

    # 设置性别
    def set_gender(self, gender):
        if gender == '男':
            # self.click_to_el(loc=self.xpath['gender_man'])
            self.select.select_by_value('0')
        elif gender == '女':
            # self.click_to_el(loc=self.xpath['gender_woman'])
            self.select.select_by_value('1')

    # 获取gender_select对象
    def create_gender_select(self):
        self.select = self.create_select(loc=self.xpath['gender_select'])

    # 点击提交
    def commit_register(self):
        self.click_to_el(loc=self.xpath['btn_commit'])

    # 等待图片加载完成
    def await_img(self):
        self.await_img_loaded(loc=self.xpath['img'])


