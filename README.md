# MyHotelProjectUIAutoTest

#### 介绍
本项目是由Selenium+Pytest+Allure+Python搭建的Web自动化测试框架，运用数据驱动测试，用于对毕设项目“酒店管理系统”进行web测试自动化实战。

#### 软件架构
- TestRun：用于存放启动自动化测试的运行脚本。
- Common：存放通用，共同的代码的。（对应第一层：通用层）
- Pages：存放通用，共同的代码的。（对应第二层：PO层）
- Business：存放业务层代码的。（对应第三层：业务层）
- TestCases：存放测试套件已经生成测试报告的代码。（对应第四层：用例层）
- Config：文件夹目录。用来存放配置信息相关的文件，如pathConfig.py等。
- Data：文件夹目录。用来存放测试数据文件的，如CSV文件等。
- Reports：文件夹目录。用来存放Allure报告文件以及日志文件。
- Tools：用来自定义一些拓展库，如对logging的二次封装库、对csv文件读取的方法等。
- 测试用例：文件夹目录。用来存放”酒店管理系统“的测试用例，以及历史Allure测试报告的备份。


#### 安装教程

1、拉取项目后，需要安装一些前置python库。在项目根目录下启动终端，运行

```powershell
pip install -r [requirements.txt文件路径]
```

2、需要安装Allure2.0。

#### 使用说明

1.  环境准备好后，运行TestRun/__init__.py即可。



#### 项目测试用例截图（摘选展示） ####

Login页测试用例

![image-20230717190520153](测试用例/Login页功能测试用例.png)

Login页缺陷记录

![image-20230717190545129](测试用例/Login页缺陷记录.png)



#### Allure测试报告截图（摘选展示） ####

报告首页

![image-20230717190243320](测试用例/Allure报告首页.png)

测试套页

![image-20230717190321719](测试用例/Allure测试套截图.png)

