import time

import pytest
import allure
from Pages.HotelNav import HotelNavPage
from selenium.webdriver.common.by import By

# 三套logger可选用
# from Tools.Logger import logger
# from Tools.NewLogger import new_logger as logger
from Tools.log import create_logger

# 引入登录业务层
from Business.LoginAction import LoginAction

# 引入登录测试数据
from Data.LoginData import testLogin


@allure.step('打开 {page_name} 页')
def open_page(open_page_function, page_name):
    open_page_function()


@allure.step('定位元素，获取el、font')
def get_el_and_font(position_function):
    el, font = position_function()
    return el, font


@allure.step('验证——文本内容')
def check_content(el, answer):
    el_text = el.text
    allure.attach(el_text, 'el_text')
    assert el_text == answer


@allure.step('验证——字体字号')
def check_font_size(font, answer):
    font_size = font['size']
    allure.attach(font_size, 'font_size')
    assert font_size == answer


@allure.step('验证——字体')
def check_font_family(font, answer):
    font_family = font['family']
    allure.attach(font_family, 'font_family')
    assert answer in font_family


@allure.step('验证——字体颜色')
def check_font_color(font, answer):
    font_color = font['color']
    allure.attach(font_color, 'font_family')
    assert answer in font_color


@allure.step('点击{btn_name}按钮')
def click_button(click_func, get_page_url_func, url_answer, btn_name):
    click_func()
    page_url = get_page_url_func()
    allure.attach(page_url, 'page_url')
    assert url_answer in page_url


@pytest.mark.usefixtures('create_driver')
@allure.feature('Hotel-Nav')
@allure.suite('导航页')
@allure.tag('导航页', '功能测试')
@allure.severity(allure.severity_level.CRITICAL)
class TestHotelNav:
    logger = create_logger('Nav', 'nav')

    @allure.story('合法（欢迎标题合法）')
    def test_hotel_nav_001(self, create_driver):
        """
        测试标题：
        合法（欢迎标题合法）

        测试步骤：
        1、比对欢迎标题，文本是否符合配置
        2、字号是否为36px
        3、字体是否为黑体
        """
        # log：打个开始log
        self.logger.info('合法（欢迎标题合法）——开始测试')
        # 创建 hotel_nav_page 页面实例
        hnp = HotelNavPage(create_driver)
        # 打开nav页面
        open_page(hnp.open_hotel_nav_page, 'Hotel-Nav')
        # 定位元素并获取el、font
        el, font = get_el_and_font(hnp.get_page_title_el)
        # 验证文本内容
        check_content(el, '欢迎使用HOTEL订房系统')
        # 验证字体字号
        check_font_size(font, '32px')
        # 验证字体
        check_font_family(font, 'sans-serif')
        # 验证字体颜色
        check_font_color(font, 'rgb(255, 255, 255)')
        # log：打个通过log
        self.logger.info('合法（欢迎标题合法）——测试通过')

    @allure.story('合法（注册按钮信息）')
    def test_hotel_nav_002(self, create_driver):
        """
        测试标题：
        合法（注册按钮信息）

        测试步骤：
        1、比对按钮文本，文本是否符合配置
        2、字号是否为16px
        3、字体是否为黑体
        """
        # log：打个开始log
        self.logger.info('合法（注册按钮信息）——开始测试')
        # 创建 hotel_nav_page 页面实例
        hnp = HotelNavPage(create_driver)
        # 打开页面
        open_page(hnp.open_hotel_nav_page, 'Hotel-Nav')
        # 定位元素并获取el、font
        el, font = get_el_and_font(hnp.get_btn_register_el)
        # 验证文本内容
        check_content(el, '注册')
        # 验证字体是否为16px
        check_font_size(font, '16px')
        # 验证字体
        check_font_family(font, 'sans-serif')
        # log：打个通过log
        self.logger.info('合法（注册按钮信息）——测试通过')

    @allure.story('合法（注册按钮跳转）')
    def test_hotel_nav_003(self, create_driver):
        """
        测试标题：
        合法（注册按钮跳转）

        测试步骤：
        1、点击注册按钮
        """
        # log：打个开始log
        self.logger.info('合法（注册按钮跳转）——开始测试')
        # 创建 hotel_nav_page 页面实例
        hnp = HotelNavPage(create_driver)
        # 打开页面
        open_page(hnp.open_hotel_nav_page, 'Hotel-Nav')
        # 点击注册按钮
        click_button(hnp.click_btn_register, hnp.get_page_url, 'register', '注册')
        # log：打个通过log
        self.logger.info('合法（注册按钮跳转）——测试通过')

    @pytest.mark.parametrize('result, user, password', testLogin)
    @allure.story('合法（登录按钮信息——已登录）')
    def test_hotel_nav_004(self, create_driver, result, user, password):
        """
        测试标题：
        合法（切换用户按钮信息）

        前置条件：
        1、登录账号
        2、返回 Nav 页

        测试步骤：
        1、比对按钮文本，文本是否符合配置
        2、字号是否为16px
        3、字体是否为黑体
        """
        # log：打个开始log
        self.logger.info('合法（登录按钮信息——已登录）——开始测试')
        # 创建 hotel_nav_page 页面实例
        hnp = HotelNavPage(create_driver)
        # 创建 LoginAction 实例
        lc = LoginAction(create_driver)
        # 登录测试账号
        token = lc.action_login_at_other_page(result=result, user=user, password=password)
        assert token != '未登录'
        # 打开页面
        open_page(hnp.open_hotel_nav_page, 'Hotel-Nav')
        # 定位切换用户按钮，获取el、font
        el, font = get_el_and_font(hnp.get_btn_login_el)
        # 验证切换用户按钮——内容
        check_content(el, '切换用户')
        # 验证字体是否为16px
        check_font_size(font, '16px')
        # 验证切换用户按钮——字体
        check_font_family(font, 'sans-serif')
        # log：打个通过log
        self.logger.info('合法（登录按钮信息——已登录）——测试通过')

    @pytest.mark.parametrize('result, user, password', testLogin)
    @allure.story('合法（登录按钮跳转——已登录）')
    def test_hotel_nav_005(self, create_driver, result, user, password):
        """
        测试标题：
        合法（切换用户按钮跳转）

        前置条件：
        1、登录账号
        2、返回 Nav 页

        测试步骤：
        1、点击切换用户按钮
        """
        # log：打个开始log
        self.logger.info('合法（登录按钮跳转——已登录）——开始测试')
        # 创建 hotel_nav_page 页面实例
        hnp = HotelNavPage(create_driver)
        # 创建 LoginAction 实例
        lc = LoginAction(create_driver)
        # 登录测试账号
        token = lc.action_login_at_other_page(result=result, user=user, password=password)
        assert token != '未登录'
        # 打开页面
        open_page(hnp.open_hotel_nav_page, 'Hotel-Nav')
        # 点击切换用户按钮
        click_button(hnp.click_btn_switch_user, hnp.get_page_url, 'login', '切换用户')
        # log：打个通过log
        self.logger.info('合法（登录按钮跳转——已登录）——测试通过')

    @allure.story('合法（登录按钮信息——未登录）')
    def test_hotel_nav_006(self, create_driver):
        """
        测试标题：
        合法（登录按钮信息）

        测试步骤：
        1、比对按钮文本，文本是否符合配置
        2、字号是否为16px
        3、字体是否为黑体
        """
        # log：打个开始log
        self.logger.info('合法（登录按钮信息——未登录）——开始测试')
        # 创建 hotel_nav_page 页面实例
        hnp = HotelNavPage(create_driver)
        # 打开页面
        open_page(hnp.open_hotel_nav_page, 'Hotel-Nav')
        # 验证是否已登录
        with allure.step('验证——是否已登录'):
            token = hnp.get_local_storage('TOKEN')
            # 如果已登录，则跳过后续验证
            if token is not None:
                return
        # 定位登录按钮，并获取el、font
        el, font = get_el_and_font(hnp.get_btn_login_el)
        # 验证按钮信息——内容
        check_content(el, '登录')
        # 验证按钮字体大小是否为16px
        check_font_size(font, '16px')
        # 验证按钮字体
        check_font_family(font, 'sans-serif')
        # log：打个通过log
        self.logger.info('合法（登录按钮信息——未登录）——测试通过')

    @allure.story('合法（登录按钮跳转——未登录）')
    def test_hotel_nav_007(self, create_driver):
        """
        测试标题：
        合法（登录按钮跳转）

        测试步骤：
        1、点击登录按钮
        """
        # log：打个开始log
        self.logger.info('合法（登录按钮跳转——未登录）——开始测试')
        # 创建 hotel_nav_page 页面实例
        hnp = HotelNavPage(create_driver)
        # 打开页面
        open_page(hnp.open_hotel_nav_page, 'Hotel-Nav')
        # 验证是否已登录
        with allure.step('验证——是否已登录'):
            token = hnp.get_local_storage('TOKEN')
            # 如果已登录，则跳过后续验证
            if token is not None:
                return
        # 点击登录按钮
        click_button(hnp.click_btn_login, hnp.get_page_url, 'login', '登录')
        # log：打个通过log
        self.logger.info('合法（登录按钮跳转——未登录）——测试通过')

    @allure.story('合法（住酒店按钮信息）')
    def test_hotel_nav_008(self, create_driver):
        """
        测试标题：
        合法（住酒店按钮信息）

        测试步骤：
        1、比对按钮文本，文本是否符合配置
        2、字号是否为16px
        3、字体是否为黑体
        """
        # log：打个开始log
        self.logger.info('合法（住酒店按钮信息）——开始测试')
        # 创建 hotel_nav_page 页面实例
        hnp = HotelNavPage(create_driver)
        # 打开页面
        open_page(hnp.open_hotel_nav_page, 'Hotel-Nav')
        # 定位住酒店按钮，并获取el、font
        el, font = get_el_and_font(hnp.get_btn_hotel_el)
        # 验证住酒店按钮——内容
        check_content(el, '住酒店')
        # 验证住酒店按钮字体大小是否为16px
        check_font_size(font, '16px')
        # 验证住酒店按钮字体
        check_font_family(font, 'sans-serif')
        # log：打个通过log
        self.logger.info('合法（住酒店按钮信息）——测试通过')

    @allure.story('合法（住酒店按钮跳转）')
    def test_hotel_nav_009(self, create_driver):
        """
        测试标题：
        合法（住酒店按钮跳转）

        测试步骤：
        1、点击住酒店按钮
        """
        # log：打个开始log
        self.logger.info('合法（住酒店按钮跳转）——开始测试')
        # 创建 hotel_nav_page 页面实例
        hnp = HotelNavPage(create_driver)
        # 打开页面
        open_page(hnp.open_hotel_nav_page, 'Hotel-Nav')
        # 点击住酒店按钮
        click_button(hnp.click_btn_hotel, hnp.get_page_url, 'goodsList', '住酒店')
        # log：打个通过log
        self.logger.info('合法（住酒店按钮跳转）——测试通过')

    @pytest.mark.parametrize('result, user, password', testLogin)
    @allure.story('合法（用户按钮信息）')
    def test_hotel_nav_010(self, create_driver, result, user, password):
        """
        测试标题：
        合法（用户按钮信息）

        前置条件：
        1、登录账号，获取user_name
        2、返回 Nav 页

        测试步骤：
        1、比对按钮文本，文本是否符合配置
        2、字号是否为16px
        3、字体是否为黑体
        """
        # log：打个开始log
        self.logger.info('合法（用户按钮信息）——开始测试')
        # 创建 hotel_nav_page 页面实例
        hnp = HotelNavPage(create_driver)
        # 创建 LoginAction 实例
        lc = LoginAction(create_driver)
        # 登录测试账号
        token = lc.action_login_at_other_page(result=result, user=user, password=password)
        assert token != '未登录'
        # 获取user_name
        user_name = lc.action_get_user_info()['userName']
        # 打开页面
        open_page(hnp.open_hotel_nav_page, 'Hotel-Nav')
        # 定位用户按钮，并获取el、font
        el, font = get_el_and_font(hnp.get_btn_user_el)
        # 验证用户按钮——内容
        check_content(el, user_name)
        # 验证用户按钮——字号
        check_font_size(font, '16px')
        # 验证用户按钮——字体
        check_font_family(font, 'sans-serif')
        # log：打个通过log
        self.logger.info('合法（用户按钮信息）——测试通过')

    @pytest.mark.parametrize('result, user, password', testLogin)
    @allure.story('合法（用户按钮跳转）')
    def test_hotel_nav_011(self, create_driver, result, user, password):
        """
        测试步骤：
        合法（用户按钮跳转）

        前置条件：
        1、登录账号
        2、返回 Nav 页

        测试步骤：
        1、点击用户按钮
        """
        # log：打个开始log
        self.logger.info('合法（用户按钮跳转）——开始测试')
        # 创建 hotel_nav_page 页面实例
        hnp = HotelNavPage(create_driver)
        # 创建 LoginAction 实例
        lc = LoginAction(create_driver)
        # 登录测试账号
        token = lc.action_login_at_other_page(result=result, user=user, password=password)
        assert token != '未登录'
        # 打开页面
        open_page(hnp.open_hotel_nav_page, 'Hotel-Nav')
        # 点击用户按钮
        click_button(hnp.click_btn_user, hnp.get_page_url, 'orders', '用户')
        # log：打个通过log
        self.logger.info('合法（用户按钮跳转）——测试通过')