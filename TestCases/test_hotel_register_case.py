import time

import pytest
import allure
from Pages.HotelRegister import HotelRegisterPage
from selenium.webdriver.common.by import By

# 三套logger可选用
# from Tools.Logger import logger
from Tools.log import create_logger
# from Tools.NewLogger import new_logger as logger

# 引入业务层
from Business.RegisterAction import RegisterAction

# 引入测试数据
from Data.RegisterData import testData, test_user

# 引入二次封装的Assert管理器
# from Tools.Asserter import AsserterTool


@allure.step('打开{page_name}页')
def open_page(open_page_func, page_name):
    open_page_func()


@allure.step('测试准备')
def ready_register(page_object):
    page_object.del_test_user(test_users=test_user)


@allure.step('定位元素，获取el、font')
def get_el_and_font(position_el_func):
    el, font = position_el_func()
    return el, font


@allure.step('验证——文本内容')
def check_content(el, answer):
    assert el.text == answer


@allure.step('等待图片加载并截图')
def await_img_and_screenshot(await_img_func, page_object):
    # 等待图片加载完成
    await_img_func()
    # 截图
    allure.attach(page_object.driver.get_screenshot_as_png(), "截图", allure.attachment_type.PNG)


@allure.step('开始进行注册——并根据需求验证数据库数据')
def register_test(page_object, user, name, password, phone, gender, result, is_check_sql):
    page_object.action_register(user=user, name=name, password=password, phone=phone, gender=gender, is_check_sql=is_check_sql)
    alert_text = page_object.get_alert_text()
    allure.attach(alert_text, '响应结果')
    assert result in alert_text


@pytest.mark.usefixtures('create_driver')
@allure.feature('Hotel-Register')
@allure.suite('注册页')
@allure.tag('注册页', '功能测试')
class TestHotelNav:
    logger = create_logger('Register', 'register')

    @allure.story('合法（UI信息）')
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.run(order=1)
    def test_hotel_register_001(self, create_driver):
        """
        测试标题：
        合法（UI信息）

        测试步骤：
        1、检查back文本是否符合文本1=注册
        2、检查title文本是否符合文本2=注册
        """
        # log：打个开始log
        self.logger.info('test_hotel_register_001——开始测试')
        # 创建 hotel_register_page 页面实例
        hrp = RegisterAction(create_driver)
        # 测试准备
        ready_register(page_object=hrp)
        # 打开页面
        open_page(hrp.open_hotel_register_page, 'Hotel-Register')
        # 定位back元素，并获取el、font
        el, font = get_el_and_font(hrp.get_page_back_el)
        # 验证back内容
        check_content(el, '注册')
        # 定位title元素，并获取el、font
        el, font = get_el_and_font(hrp.get_page_title_el)
        # 验证title内容
        check_content(el, '注册')
        # 等待图片加载完成并截图保存为Allure报告附件
        await_img_and_screenshot(hrp.await_img, hrp)
        # log：打个通过log
        self.logger.info('test_hotel_register_001——测试通过')

    @pytest.mark.parametrize('title, result, user, name, password, phone, gender, is_check_sql', testData)
    @allure.story('注册测试')
    @allure.title('{title}')
    def test_hotel_register_002(self, create_driver, title, result, user, name, password, phone, gender, is_check_sql):
        """
        测试步骤：
        1、输入user
        2、输入name
        3、输入password
        4、输入phone
        5、选择gender
        6、点击提交
        """
        # log：打个开始log
        self.logger.info(f'{title}——开始测试')
        # 创建 hotel_register_page 页面实例
        hrp = RegisterAction(create_driver)
        # 打开页面
        open_page(hrp.open_hotel_register_page, 'Hotel-Register')
        # 按测试步骤进行测试
        register_test(hrp, user=user, name=name, password=password, phone=phone, gender=gender, result=result, is_check_sql=is_check_sql)
        # log：打个通过log
        self.logger.info(f'{title}——测试通过')
