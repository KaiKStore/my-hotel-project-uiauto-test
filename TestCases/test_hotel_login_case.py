import pytest
import allure

# 三套logger可选用
# from Tools.Logger import logger
# from Tools.NewLogger import new_logger as logger
from Tools.log import create_logger

# 引入业务层
from Business.LoginAction import LoginAction

# 引入测试数据
from Data.LoginData import testData
from Data.LoginData import testLogin
from Data.LoginData import testSwitchUser


@allure.step('打开{page_name}页')
def open_page(open_page_func, page_name):
    open_page_func()


@allure.step('定位元素并获取el、font')
def get_el_and_font(get_el_and_font_func):
    el, font = get_el_and_font_func()
    return el, font


@allure.step('判断元素文本内容')
def check_el_content(el, answer):
    allure.attach(el.text, 'el_text')
    allure.attach(answer, 'answer')
    assert el.text == answer


@allure.step('等待图片元素加载完成进行截图')
def await_img_to_screenshot(page_object):
    # 等待图片加载完成
    page_object.await_img()
    # 截图并存放到allure报告中
    allure.attach(page_object.driver.get_screenshot_as_png(), "截图", allure.attachment_type.PNG)


@allure.step('进行登录测试——{title}')
def login_test(page_object, result, user, password, title):
    page_object.action_login(result=result, user=user, password=password)
    token = page_object.action_get_token()
    allure.attach(token, '响应结果')


@pytest.mark.usefixtures('create_driver')
@allure.feature('Hotel-Login')
@allure.suite('登录页')
@allure.tag('登录页', '功能测试')
class TestHotelNav:
    logger = create_logger('Login', 'login')

    @allure.story('合法（UI信息——未登录）')
    @allure.severity(allure.severity_level.CRITICAL)
    def test_hotel_login_001(self, create_driver):
        """
        测试标题：
        UI信息合法（未登录）
        测试步骤：
        1、检查back文本是否符合文本1=登录
        2、检查title文本是否符合文本2=登录
        """
        # log：打个开始log
        self.logger.info('test_hotel_login_001——开始测试')
        # 创建 hotel_register_page 页面实例
        hlp = LoginAction(create_driver)
        # 打开页面
        open_page(hlp.open_hotel_login_page, 'Hotel-Login')
        # 获取back元素el、font
        el, font = get_el_and_font(hlp.get_page_back_el)
        # 验证back文本是否为登录
        check_el_content(el, '登录')
        # 获取title元素el、font
        el, font = get_el_and_font(hlp.get_page_title_el)
        # 验证title文本是否为登录
        check_el_content(el, '登录')
        # 等待图片加载完成并截图
        await_img_to_screenshot(hlp)
        # log：打个通过log
        self.logger.info('test_hotel_login_001——开始通过')

    @pytest.mark.parametrize('result, user, password', testLogin)
    @allure.story('合法（UI信息——已登录）')
    @allure.severity(allure.severity_level.CRITICAL)
    def test_hotel_login_002(self, create_driver, result, user, password):
        """
        测试标题：
        UI信息合法（已登录）
        测试步骤：
        1、检查back文本是否符合文本1=登录
        2、检查title文本是否符合文本2=切换用户
        """
        # log：打个开始log
        self.logger.info(f'登录测试——{result}-{user}-{password}——开始测试')
        # 创建 hotel_register_page 页面实例
        hlp = LoginAction(create_driver)
        # 打开页面
        open_page(hlp.open_hotel_login_page, 'Hotel-Login')
        # 登录测试账号
        hlp.action_login_test_account(result=result, user=user, password=password)
        # 获取测试账号token
        token = hlp.action_get_token()
        # 重新打开页面
        open_page(hlp.open_hotel_login_page, 'Hotel-Login')
        # 定位back，获取el、font
        el, font = get_el_and_font(hlp.get_page_back_el)
        # 验证back文本内容是否为"登录"
        check_el_content(el, '登录')
        # 定位title，获取el、font
        el, font = get_el_and_font(hlp.get_page_title_el)
        # 验证title文本内容是否为"切换用户"
        check_el_content(el, '切换用户')
        # 等待图片加载完成后进行截图
        await_img_to_screenshot(hlp)
        # log：打个通过log
        self.logger.info(f'登录测试——{result}-{user}-{password}——开始通过')

    @pytest.mark.parametrize('result1, user1, password1, result2, user2, password2', testSwitchUser)
    @allure.story('登录成功（切换用户）')
    @allure.severity(allure.severity_level.CRITICAL)
    def test_hotel_login_004(self, create_driver, result1, user1, password1, result2, user2, password2):
        """
        测试标题：
        UI信息合法（已登录）
        测试步骤：
        1、登录账号1后重新进入登录页
        2、登录账号2
        """
        # log：打个开始log
        self.logger.info('切换账号——开始测试')
        # 创建 hotel_register_page 页面实例
        hlp = LoginAction(create_driver)
        # 打开页面
        open_page(hlp.open_hotel_login_page, 'Hotel-Login')
        # 登录测试账号
        hlp.action_login_test_account(result=result1, user=user1, password=password1)
        # 获取测试账号token
        token = hlp.action_get_token()
        # 重新打开页面
        open_page(hlp.open_hotel_login_page, 'Hotel-Login')
        # 登录测试账号
        hlp.action_login_test_account(result=result2, user=user2, password=password2)
        # 获取测试账号token
        token = hlp.action_get_token()
        # log：打个通过log
        self.logger.info('切换账号——测试通过')

    @pytest.mark.parametrize('title, result, user, password', testData)
    @allure.story('登录测试')
    @allure.title('{title}')
    def test_hotel_login_003(self, create_driver, title, result, user, password):
        """
        测试步骤：
        1、输入name
        2、输入password
        3、点击提交
        """
        # log：打个开始log
        self.logger.info(f'{title}——开始测试')
        # 创建 hotel_register_page 页面实例
        hrp = LoginAction(create_driver)
        # 打开页面
        open_page(hrp.open_hotel_login_page, 'Hotel-Login')
        # 进行登录测试
        login_test(hrp, result, user, password, title)
        # log：打个通过log
        self.logger.info(f'{title}——测试通过')

