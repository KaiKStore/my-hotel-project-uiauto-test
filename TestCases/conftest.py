from selenium import webdriver
# Edge驱动
from webdriver_manager.microsoft import EdgeChromiumDriverManager
# Chrome驱动
# from webdriver_manager.chrome import ChromeDriverManager
import pytest


@pytest.fixture(scope='function', autouse=True)
def create_driver():
    driver = webdriver.Edge()
    yield driver
    driver.quit()
