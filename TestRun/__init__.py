import pytest
import datetime
import os
from config.pathConfig import report_path, allure_results_path, allure_report_path


def run_all():
    # times = datetime.datetime.now().strftime('%Y-%m-%d %H-%M-%S')
    AllureUseTimes = datetime.datetime.now().strftime('%Y%m%d-%H-%M-%S')

    # 生成pytest-html报告文件（因为现在改用Allure报告，该方式不用了。）
    # report_name = os.path.join(report_path ,(times+'report.html'))
    # pytest.main(['-s', '-v', '-n 2', '-reruns 2', '--html={}'.format(report_name)])

    # 运行pytest，生成Allure临时数据文件
    pytest.main(['-s', '-v', '-n 3', '-reruns 2', '--alluredir={}'.format(allure_results_path)])

    # 运行CMD命令，将Allure临时数据文件转换为报告文件
    os.system(f'allure generate {allure_results_path} -o {allure_report_path}/{AllureUseTimes}-html --clean')


if __name__ == '__main__':
    run_all()


