"""
    Des：读取CSV文件并转化为list返回
    ——dtype指定读取的数据类型为字符串，因为默认是浮点型读取，会让数据莫名带1位小数点。
    ——fillna('')：用来将csv中为空的项转换位''，避免出现空项转为nan问题。
    ——header：csv文件默认会将第一行设置为header，并且不读取。如果配置的csv中设计上不带header，那么需要配置header=None，又如果csv设计上
    第二行才是header，那么就配置header=1（从0开始）。
    ——注意：csv文件是不支持多个工作表读取的。
"""
import pandas as pd


# 读取csv数据，从第一行读起
def load_csv(path):
    data = pd.read_csv(path, encoding='GBK', dtype=str, header=None).fillna('').values.tolist()
    return data


# 读取csv数据，从第二行读起（第一行为表头）
def load_csv_skip_row(path):
    data = pd.read_csv(path, encoding='GBK', dtype=str).fillna('').values.tolist()
    return data
