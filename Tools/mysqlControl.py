"""
    Des：mysql控制器，用于操作mysql。
"""
import mysql.connector


# 连接数据库并返回游标
def use_mysql():
    # 连接数据库
    cnx = mysql.connector.connect(
        host='localhost',
        user='root',
        password='123456',
        port='3306',
        database='my_school_project_database'
    )
    # 设置自动提交，这样在外层使用增删改就不需要将cnx返回出去了
    cnx.autocommit = True
    # yield cnx
    # 创建游标对象
    cursor = cnx.cursor()
    yield cursor
    # 结束游标
    cursor.close()
    # 关闭mysql连接
    cnx.close()