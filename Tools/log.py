"""
    CreateDate：2023年7月16日
    Author：开朗的不得了
    Des：第三套二次封装的log库
"""
import logging
from config.pathConfig import logs_path
import datetime


def create_logger(logger_name, module_name):
    # 创建时间
    date = datetime.datetime.now().strftime('%Y%m%d-%H-%M-%S')

    # 创建日志记录器
    logger = logging.getLogger(date+"-"+logger_name)
    logger.setLevel(logging.DEBUG)

    # 创建文件处理器
    file_handler = logging.FileHandler(logs_path + f'\\{date}—{module_name}.log', encoding='utf-8')
    file_handler.setLevel(logging.DEBUG)

    # 创建控制台处理器
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)

    # 创建日志格式化器
    formatter = logging.Formatter('%(asctime)s %(filename)s.%(funcName)s[line:%(lineno)d] %(levelname)s %(message)s')

    # 将格式化器添加到处理器
    file_handler.setFormatter(formatter)
    console_handler.setFormatter(formatter)

    # 将处理器添加到记录器中
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)

    # 返回logger器
    return logger