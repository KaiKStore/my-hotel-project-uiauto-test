"""
    Des：自定义封装的日志模块
    Date：2023年6月14日18点19分
    Update：2023年6月14日18点19分
    Author：开朗的不得了

"""
import logging
import logging.config
import sys, os
from config.pathConfig import tools_path


def findcaller(func):
    # 装饰器findcaller，用于获取调用日志代码所在的函数名、文件名、行数等信息。
    def wrapper(*args):
        f=sys._getframe()
        filename=f.f_back.f_code.co_filename
        funcname=f.f_back.f_code.co_name
        lineno=f.f_back.f_lineno
        args=list(args)
        args.append('%s.%s.%s' % (os.path.basename(filename),funcname,lineno))
        func(*args)
    return wrapper


# 日志类Result
class Result(object):
    def __init__(self):
        # 初始化os配置
        logging.config.fileConfig(os.path.join(tools_path,'logger.conf'), encoding='utf-8')
        self.resulter = logging.getLogger('result')
        self.infor = logging.getLogger('infomation')

    @findcaller
    def log_pass(self, caller=''):
        self.resulter.critical('PASS::'+caller)

    @findcaller
    def log_fail(self, caller=''):
        self.resulter.critical('FAIL::' + caller)

    @findcaller
    def log_debug(self, msg, caller=''):
        self.infor.debug('[%s] %s' %(caller, msg))

    @findcaller
    def log_info(self, msg, caller=''):
        self.infor.info('[%s] %s' %(caller, msg))

    @findcaller
    def log_warning(self, msg, caller=''):
        self.infor.warning('[%s] %s' % (caller, msg))

    @findcaller
    def log_error(self, msg, caller=''):
        self.infor.error('[%s] %s' % (caller, msg))

    @findcaller
    def log_critical(self, msg, caller=''):
        self.infor.critical('[%s] %s' % (caller, msg))


logger = Result()
