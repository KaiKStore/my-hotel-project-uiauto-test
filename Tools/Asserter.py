"""
    CreateDate：2023年7月14日 20点46分
    Author：开朗的不得了
    Des：二次封装断言器，当断言发现错误时进行截图

"""
import allure


class AsserterTool(object):
    assert_el = ''
    answer = ''
    assert_type = ''

    def __init__(self, assert_el, answer, assert_type, driver):
        self.assert_el = assert_el
        self.answer = answer
        self.assert_type = assert_type
        self.assert_manager(driver=driver)

    # assert管理器——出错抓截图
    def assert_manager(self, driver):
        try:
            if self.assert_type == 0:
                self.assert_equate(el=self.assert_el, answer=self.answer)
            elif self.assert_type == 1:
                self.assert_not_equate(el=self.assert_el, answer=self.answer)
            elif self.assert_type == 2:
                self.assert_include(el=self.assert_el, keyword=self.answer)
        except AssertionError:
            allure.attach(driver.get_screenshot_as_png(), "出错截图", allure.attachment_type.PNG)
            raise AssertionError('断言失败，{0}，请看截图'.format(AssertionError))

    # assert——condition：是否等于 (Type==0)
    def assert_equate(self, el, answer):
        assert el == answer

    # assert——condition：是否不等于 (Type==1)
    def assert_not_equate(self, el, answer):
        assert el != answer

    # assert——condition：是否包含 (Type==2)
    def assert_include(self, el, keyword):
        assert keyword in el



