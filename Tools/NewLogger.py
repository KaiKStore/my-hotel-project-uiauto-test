"""
    Des：第二套封装的日志模块（loguru）
    Date：2023年6月15日23点59分
    Update：2023年6月15日23点59分
    Author：开朗的不得了

"""
from loguru import logger
from config.pathConfig import log_info_path


class Result():

    def __init__(self, file_path):
        logger.add(sink=file_path, encoding='utf-8')
        self.logger = logger

    def log_info(self, msg):
        self.logger.info(msg)

    def log_warning(self, msg):
        self.logger.warning(msg)

    def log_critical(self, msg):
        self.logger.critical(msg)

    def log_error(self, msg):
        self.logger.error(msg)

    def log_debug(self, msg):
        self.logger.error(msg)

    def log_pass(self):
        self.log_pass()


new_logger = Result(log_info_path)