"""
    Date：2023年6月15日20点00分
    Author：开朗的不得了
    Des：路径统一配置

"""
import os

# 当前文件目录路径
current_dir = os.path.abspath(os.path.dirname(__file__))
# 项目目录路径
project_path = os.path.abspath(os.path.join(current_dir, os.path.pardir))
# Tools路径
tools_path = os.path.abspath(os.path.join(project_path, "Tools"))
# Report路径
report_path = os.path.abspath(os.path.join(project_path, "Reports"))
# Allure_temp临时数据文件路径
allure_results_path = os.path.join(report_path, 'allure-results')
# Allure_report文件路径
allure_report_path = os.path.join(report_path, 'allure-reports')
# log路径
logs_path = os.path.abspath(os.path.join(report_path, "logs"))
# log_info路径
log_info_path = os.path.join(logs_path, 'log_info.txt')

# print("@@@", current_dir)
# print('@@@1', project_path)
# print('@@@2', tools_path)
# print('@@@3', report_path)
# print('@@@4', logs_path)