import sys
import os
sys.path.append(os.path.abspath(os.path.dirname(__file__)))
import pathConfig as pc

sys.path.append(pc.project_path)
sys.path.append(pc.logs_path)
sys.path.append(pc.tools_path)
sys.path.append(pc.report_path)