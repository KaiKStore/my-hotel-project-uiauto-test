import json
import time

from Pages.HotelLogin import HotelLoginPage
import allure


class LoginAction(HotelLoginPage):
    token = '未登录'
    user_info = ''

    # 进行登录，并返回登录成功后的token（内页使用）
    def action_login(self, result, user, password):
        self.set_ipt_user(user)
        self.set_ipt_password(password)
        self.commit_login()
        self.get_token(result=result)

    # 验证登录结果是否符合预期
    def check_result(self, result):
        alert_text = self.get_alert_text()
        allure.attach(alert_text, 'alert_text')
        assert result in alert_text

    # 返回测试账号的token
    def get_account_token(self):
        # 执行JavaScript，获取token
        time.sleep(2)
        script = "return window.localStorage.getItem('TOKEN')||'未登录'"
        token = self.driver.execute_script(script)
        allure.attach(token, 'token')
        self.token = token

    # 等待登录结果，并获取token
    def get_token(self, result):
        self.check_result(result=result)
        self.get_account_token()

    # 获取账号的userInfo
    def get_user_info(self):
        script = "return window.localStorage.getItem('USERINFO')||'未登录'"
        user_info_str = self.driver.execute_script(script)
        allure.attach(user_info_str, 'user_info_str')
        self.user_info = json.loads(user_info_str)

    # 业务操作：登录测试账号
    @allure.step('进行登录')
    def action_login_test_account(self, result, user, password):
        # 进行登录，并将测试账号的token存放到类中
        self.action_login(result, user, password)

    # 业务操作：获取token
    @allure.step('获取token')
    def action_get_token(self):
        allure.attach(self.token, 'token')
        return self.token

    # 业务操作：获取user_info
    @allure.step('获取user_info')
    def action_get_user_info(self):
        allure.attach(str(self.user_info), 'user_info')
        return self.user_info

    # 进行登录，并返回登录成功后的token（外页使用，一键登录并返回token）
    @allure.step('一键登录并返回token')
    def action_login_at_other_page(self, result, user, password):
        self.open_hotel_login_page()  # 打开页面
        self.action_login(result=result, user=user, password=password)
        allure.attach(self.token, 'token')
        self.get_user_info()
        return self.token