from Tools.csvReader import load_csv_skip_row
# （标题， 对比的结果， 0：获取token 1：不获取token， 账号， 密码）
# testData = [
#     ('登录成功（全部正确）', '成功', 'abc1', '123456'),
#     ('登录失败（账号未注册+其他正确）', '账号', 'abc123', '123456'),
#     ('登录失败（账号为空+其他正确）', '账号', '', '123456'),
#     ('登录失败（密码为空+其他正确）', '账号密码不一致', 'abc1', ''),
#     ('登录失败（密码错误+其他正确）', '账号密码不一致', 'abc1', '123457'),
# ]
# testLogin = [
#     ('成功', 'abc1', '123456')
# ]
# testSwitchUser = [
#     ('成功', 'abc1', '123456', '成功', 'abc0', '123456')
# ]

testData = load_csv_skip_row('D:\\Users\\Documents\\myGitee\\my-store\\my-hotel-project-uiauto-test\\Data\\CSV\\LoginData.csv')
testLogin = load_csv_skip_row('D:\\Users\\Documents\\myGitee\\my-store\\my-hotel-project-uiauto-test\\Data\\CSV\\LoginUser.csv')
testSwitchUser = load_csv_skip_row('D:\\Users\\Documents\\myGitee\\my-store\\my-hotel-project-uiauto-test\\Data\\CSV\\LoginSwitchUser.csv')